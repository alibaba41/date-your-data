setwd("C:/Users/syed danish/Desktop/r/date ur data")
train=read.csv("train.csv")
View(train)
test=read.csv("test.csv")
student=read.csv("Student.csv")
View(test)
View(train)
View(student)
internship=read.csv("Internship.csv")
View(internship)
require(plyr)
train=join(x=train,y=student,by="Student_ID",match="first")
train=join(x=train,y=internship,by="Internship_ID",match="first")
test=join(x=test,y=student,by="Student_ID",match="first")
test=join(x=test,y=internship,by="Internship_ID",match="first")
train$Performance_PG=train$Performance_PG/train$PG_scale
test$Performance_PG=test$Performance_PG/test$PG_scale
train$Performance_UG=train$Performance_UG/train$UG_Scale
test$Performance_UG=test$Performance_UG/test$UG_Scale
m1=train
m2=test
m1$Earliest_Start_Date=NULL
m1$Expected_Stipend=NULL
m1$Preferred_location=NULL
m1$Institute_location=NULL
m1$hometown=NULL
m1$Degree=NULL
m1$Stream=NULL
m1$Experience_Type=NULL
m1$Profile=NULL
m1$Location=NULL
m1$Start.Date=NULL
m1$End.Date=NULL
m1$Internship_Profile=NULL
m1$Internship_Profile=NULL
m1$Skills_required=NULL
m1$Internship_Location=NULL
m1$Stipend_Type=NULL
m1$Stipend2=NULL
m1$Internship_deadline=NULL
m1$Start_Date=NULL



m2$Earliest_Start_Date=NULL
m2$Expected_Stipend=NULL
m2$Preferred_location=NULL
m2$Institute_location=NULL
m2$hometown=NULL
m2$Degree=NULL
m2$Stream=NULL
m2$Experience_Type=NULL
m2$Profile=NULL
m2$Location=NULL
m2$Start.Date=NULL
m2$End.Date=NULL
m2$Internship_Profile=NULL
m2$Internship_Profile=NULL
m2$Skills_required=NULL
m2$Internship_Location=NULL
m2$Stipend_Type=NULL
m2$Stipend2=NULL
m2$Internship_deadline=NULL
m2$Start_Date=NULL
m1$Internship_ID=NULL
m1$Student_ID=NULL
m2$Student_ID=NULL
m2$Internship_ID=NULL
m1$PG_scale=NULL
m1$UG_Scale=NULL
m2$PG_scale=NULL
m2$UG_Scale=NULL

m_train=m1
m_test=m2
m_train=m_train[,c(1:15)]
m_test=m_test[,c(1:14)]

m_train$Stipend1=as.numeric(m_train$Stipend1)
m_test$Stipend1=as.numeric(m_test$Stipend1)

m_train$Institute_Category=as.numeric(m_train$Institute_Category)
m_test$Institute_Category=as.numeric(m_test$Institute_Category)

m_train$Current_year=as.numeric(m_train$Current_year)
m_test$Current_year=as.numeric(m_test$Current_year)

m_train$Internship_Type=as.numeric(m_train$Internship_Type)
m_test$Internship_Type=as.numeric(m_test$Internship_Type)

m_train$Internship_category=as.numeric(m_train$Internship_category)
m_test$Internship_category=as.numeric(m_test$Internship_category)

label=m_train$Is_Shortlisted
m_train$Is_Shortlisted=NULL
require(xgboost)
bst=xgboost(data = as.matrix(m_train),label = label,max.depth = 4,eta = 1, nthread = 2, nround = 10,objective = "binary:logistic")

pred=predict(bst,as.matrix(m_test))
pred_5=as.numeric(pred>.1135)
table(pred_5)
MySubmission = data.frame(Internship_ID=test$Internship_ID,Student_ID=test$Student_ID,Is_Shortlisted=pred_5)
write.csv(MySubmission, "Submissionbst.csv", row.names=FALSE)
